package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int sizeInput;
        try {
            sizeInput = inputNumbers.size();
        } catch (java.lang.Exception e) {
            throw new CannotBuildPyramidException();
        }
        for (int i = 0; i < sizeInput; i++) {
            if (inputNumbers.get(i) == null) {
                throw new CannotBuildPyramidException();
            }
        }

        int sizeStr = (-1 + (int)Math.sqrt(1 + 8 * sizeInput)) / 2;

        if ((Math.abs(-1 + Math.sqrt(1 + 8 * sizeInput)) / 2 - sizeStr) > 0.00001)
            throw new CannotBuildPyramidException();

        try {
            Collections.sort(inputNumbers);
        } catch (java.lang.OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = new int[sizeStr][2 * sizeStr - 1];

        int indInput = 0;
        int indAns = sizeStr - 1;
        for (int i = 0; i < sizeStr; i++) {
            int start = indAns;
            for (int j = 0; j <= i; j++) {
                pyramid[i][start] = inputNumbers.get(indInput);
                indInput++;
                start += 2;
            }
            indAns--;
        }

        return pyramid;
    }


}
