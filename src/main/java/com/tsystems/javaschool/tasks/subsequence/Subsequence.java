package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        int lengthX, lengthY;
        try {
            lengthX = x.size();
            lengthY = y.size();
        } catch (java.lang.Exception e) {
            throw new IllegalArgumentException();
        }
        ArrayList<ArrayList<Integer>> indices = new ArrayList<>(lengthX);
        for (int i = 0; i < lengthX; i++) {
            ArrayList<Integer> buf = new ArrayList<>();
            for (int j = 0; j < lengthY; j++) {
                if (x.get(i).equals(y.get(j))) {
                    buf.add(j);
                }
            }
            indices.add(buf);
        }

        final int max = Integer.MAX_VALUE - 1;
        int prev = -1;
        for (int i = 0; i < indices.size(); i++) {
            int element = max;
            for (int j = 0; j < indices.get(i).size(); j++) {
                if (indices.get(i).get(j) < element && indices.get(i).get(j) > prev)
                    element = indices.get(i).get(j);
            }

            if (element != max){
                prev = element;
            } else {
                return false;
            }
        }

        return true;
    }
}
