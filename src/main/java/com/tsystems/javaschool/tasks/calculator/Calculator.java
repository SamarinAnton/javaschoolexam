package com.tsystems.javaschool.tasks.calculator;

import java.util.HashMap;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
            if (statement.equals(""))
                return null;
        } catch (java.lang.NullPointerException e) {
            return null;
        }

        HashMap<String, Integer> priority = new HashMap<>();
        priority.put("(", 0);
        priority.put(")", 0);
        priority.put("+", 1);
        priority.put("-", 1);
        priority.put("*", 2);
        priority.put("/", 2);

        Stack<String> stack = new Stack<>();
        Stack<String> strInOpn = new Stack<>();

        boolean operator = false;
        for (int i = 0; i < statement.length(); i++) {
            Boolean itsDiv = false;
            char c = statement.charAt(i);
            StringBuffer number = new StringBuffer();

            if (Character.isDigit(c)) {
                operator = false;

                while (Character.isDigit(c) || c == '.') {
                    if (c == '.') {
                        if (itsDiv)
                            return null;
                        itsDiv = true;
                    }
                    number.append(c);
                    if (i < statement.length() - 1) {
                        i++;
                        c = statement.charAt(i);
                    } else
                        break;
                }
                strInOpn.push(new String(number));
            }

            if (i == statement.length() - 1 && Character.isDigit(c))
                break;

            String s = String.valueOf(c);

            if (s.equals("(")) {
                stack.push(s);
            } else {
                if (s.equals(")")) {
                    String buf = stack.pop();
                    while (!buf.equals("(")) {
                        strInOpn.push(buf);
                        try {
                            buf = stack.pop();
                        } catch (java.util.EmptyStackException e) {
                            return null;
                        }
                    }
                } else {
                    if (priority.get(s) == null || operator)
                        return null;

                    operator = true;

                    if (!stack.empty()) {
                        while (!stack.empty() && priority.get(s) <= priority.get(stack.peek())) {
                            strInOpn.push(stack.pop());
                        }
                    }

                    stack.push(s);
                }
            }

        }

        int lastAdd = stack.size();
        for (int i = 0; i < lastAdd; i++) {
            String op = stack.pop();
            if (op.equals("("))
                return null;
            strInOpn.push(op);
        }

        HashMap<String, DoubleBinaryOperator> binaryOperators = new HashMap<>();
        binaryOperators.put("+", (a, b) -> a + b);
        binaryOperators.put("-", (a, b) -> a - b);
        binaryOperators.put("*", (a, b) -> a * b);
        binaryOperators.put("/", (a, b) -> a / b);

        stack.clear();
        for (int i = 0; i < strInOpn.size(); i++) {
            String str = strInOpn.get(i);
            double d;
            try {
                d = Double.parseDouble(str);
            } catch (NumberFormatException e) {
                double a = Double.parseDouble(stack.pop());
                double b = Double.parseDouble(stack.pop());
                double x = binaryOperators.get(str).applyAsDouble(b, a);
                String buf = Double.toString(x);
                if (buf.equals("Infinity"))
                    return null;
                stack.push(buf);
                continue;
            }
            stack.push(Double.toString(d));
        }

        double ans = Double.parseDouble(stack.pop());
        if (Math.abs(ans - (int)ans) > 0.00001) {
            ans = Math.ceil(ans * 10000) / 10000;
            stack.push((Double.toString(ans)));
        } else {
            stack.push(Integer.toString((int)ans));
        }

        return stack.pop();
    }

}
